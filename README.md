# Blanco project

This is a Blanco project template that can be used to start a new project in AskAnna.
This template only contain an `askanna.yml` file.

Using the AskAnna CLI you can run `askanna create` to create a new project in AskAnna.
By default this Blanco template is used. If creating the project is done, you will find
a new local directory with a `askanna.yml` that already contain the url of the new
project.
